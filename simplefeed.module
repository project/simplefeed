<?php

/**
 * @file
 * Parse feeds into nodes.
 */

/**
 * Implementation of hook_init().
 */
function simplefeed_init() {
  // ensure we are not serving a cached page
  if (function_exists('drupal_set_content')) {
    // we don't do this in hook_menu to ensure the files are already included when
    // views_menu is executed
    if (module_exists('views')) {
      include_once('./'. drupal_get_path('module', 'simplefeed') .'/simplefeed_views.inc');
    }
    if (module_exists('token')) {
      include_once('./'. drupal_get_path('module', 'simplefeed') .'/simplefeed_token.inc');
    }
  }
}


/**
 * Implementation of hook_menu().
 */
function simplefeed_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/simplefeed',
      'title' => t('SimpleFeed'),
      'description' => t('Configure how feeds are parsed.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('simplefeed_admin_settings'),
      'access' => user_access('administer feeds'),
    );
  }
  else {
    $items[] = array(
      'path' => 'feed/refresh/'. arg(2),
      'title' => t('Refresh feed'),
      'callback' => 'simplefeed_feed_refresh',
      'callback arguments' => array(arg(2)),
      'access' => user_access('administer feeds'),
      'type' => MENU_CALLBACK
    );

    $items[] = array(
      'path' => 'feed/empty/'. arg(2),
      'title' => t('Empty feed'),
      'callback' => 'simplefeed_feed_empty',
      'callback arguments' => array(arg(2)),
      'access' => user_access('administer feeds'),
      'type' => MENU_CALLBACK
    );
  }

  return $items;
}


/**
 * Implementation of hook_perm().
 */
function simplefeed_perm() {
  return array('create feeds', 'edit own feeds', 'edit feeds', 'administer feeds');
}


/**
 * Implementation of hook_access().
 */
function simplefeed_access($op, $node) {
  global $user;

  switch ($op) {
    case 'create':
      return user_access('create feeds');
      break;

    case 'update':
    case 'delete':
      // users who create a feed may edit or delete it later, assuming they have the necessary permissions
      if ((user_access('edit own feeds') && ($user->uid == $node->uid)) || user_access('edit feeds')) {
        return TRUE;
      }
      break;
  }
}


/**
 * Implementation of hook_node_info().
 */
function simplefeed_node_info() {
  return array(
    'feed' => array(
      'name' => t('Feed'),
      'module' => 'simplefeed',
      'description' => t('Aggregate an RSS or Atom syndication feed.'),
      'body_label' => t('Description'),
    ),
  );
}


/**
 * Implementation of hook_form().
 */
function simplefeed_form(&$node) {
  $type = node_get_types('type', $node);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5,
  );
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#default_value' => $node->body,
    '#rows' => 3,
    '#required' => TRUE,
  );
  $form['body_filter']['format'] = filter_form($node->format);

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The URL for this feed.'),
    '#default_value' => isset($node->url) ? $node->url : 'http://www.',
    '#maxlength' => 255,
    '#required' => TRUE,
  );


  if (user_access('administer feeds')) {
    $form['feed_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Feed settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $period = array(0 => t('Never')) + drupal_map_assoc(array(3600, 10800, 21600, 32400, 43200, 86400, 172800, 259200, 604800, 1209600, 2419200, 4838400, 9676800), 'format_interval');
    $form['feed_settings']['expires'] = array(
      '#type' => 'select',
      '#title' => t('Discard feed items older than'),
      '#default_value' => isset($node->expires) ? $node->expires : variable_get('simplefeed_expires', 86400),
      '#options' => $period,
      '#description' => t('Older feed items will be automatically discarded. Requires !cron to be running.', array('!cron' => l('cron', 'admin/logs/status'))),
    );

    $period = array(0 => t('Never'), 1 => t('Manual')) + drupal_map_assoc(array(900, 1800, 3600, 7200, 10800, 21600, 32400, 43200, 64800, 86400, 172800, 259200, 604800), 'format_interval');
    $form['feed_settings']['refresh'] = array(
      '#type' => 'select',
      '#title' => t('Check feed every'),
      '#description' => t('How often should a feed be checked for new content? If you select "manual", you can update a feed by clicking "refresh this feed" below the feed node. If you select a time, !cron must be running.', array('!cron' => l('cron', 'admin/logs/status'))),
      '#options' => $period,
      '#default_value' => isset($node->refresh) ? $node->refresh : variable_get('simplefeed_refresh', 3600),
    );
  }
  else {
    $form['feed_settings']['expires'] = array('#type' => 'value', '#value' => variable_get('simplefeed_expires', 86400));
    $form['feed_settings']['refresh'] = array('#type' => 'value', '#value' => variable_get('simplefeed_refresh', 3600));
  }

  return $form;
}


/**
 * Implementation of hook_validate().
 */
function simplefeed_validate($node) {
  $valid_url = "`(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?`";
  $url = trim($node->url);

  if (preg_match($valid_url, $url)) {
    $duplicate = db_result(db_query("SELECT nid FROM {simplefeed_feed} WHERE url = '%s'", $node->url));

    // make sure this URL does not exist already
    // if this node is being updated make sure it doesn't trigger a duplicate warning
    if ($duplicate != '' && $duplicate != $node->nid) {
      form_set_error('url', t('The URL entered already exists. Please try a different URL.'));
    }
  }
  else {
    form_set_error('url', t('The URL entered is not valid. It should be in the format of: %url_format', array('%url_format' => 'http://www.example.com/rss.xml')));
  }
}


/**
 * Implementation of hook_load().
 */
function simplefeed_load($node) {
  $additions = db_fetch_object(db_query('SELECT url, expires, refresh, checked FROM {simplefeed_feed} WHERE vid = %d', $node->vid));
  return $additions;
}


/**
 * Implementation of hook_insert().
 */
function simplefeed_insert($node) {
  db_query("INSERT INTO {simplefeed_feed} (vid, nid, url, expires, refresh) VALUES (%d, %d, '%s', %d, %d)", $node->vid, $node->nid, $node->url, $node->expires, $node->refresh);
}


/**
 * Implementation of hook_update().
 */
function simplefeed_update($node) {
  // if this is a new node or we're adding a new revision
  if ($node->revision) {
    simplefeed_insert($node);
  }
  else {
    db_query("UPDATE {simplefeed_feed} SET url = '%s', expires = %d, refresh = %d WHERE vid = %d", $node->url, $node->expires, $node->refresh, $node->vid);
  }
}


/**
 * Implementation of hook_delete().
 */
function simplefeed_delete($node) {
  // delete the feed
  db_query('DELETE FROM {simplefeed_feed} WHERE nid = %d', $node->nid);

  // delete all feed items associated with this feed
  $feed_items = db_query('SELECT nid FROM {simplefeed_feed_item} WHERE fid = %d', $node->nid);

  while ($feed_item = db_fetch_object($feed_items)) {
    node_delete($feed_item->nid);
  }

  db_query('DELETE FROM {simplefeed_feed_item} WHERE fid = %d', $node->nid);
}


/**
 * Implementation of hook_nodeapi().
 */

function simplefeed_nodeapi(&$node, $op, $teaser, $page) {
  switch ($op) {
    case 'delete revision':
      db_query('DELETE FROM {simplefeed_feed} WHERE vid = %d', $node->vid);
      break;
  }
}


/**
 * Implementation of hook_view().
 */
function simplefeed_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);

  $node->content['simplefeed']['#theme'] = 'simplefeed_node_view';
  $node->content['simplefeed']['#weight'] = 2;
  $node->content['simplefeed']['url'] = array(
    '#value' => check_url($node->url),
    '#weight' => 1,
  );

  // since only administrators can edit when a feed expires, only admins can see what this value is
  if (user_access('administer feeds')) {
    $node->content['simplefeed']['expires'] = array(
      '#value' => format_interval($node->expires),
    );
    $node->content['simplefeed']['refresh'] = array(
      '#value' => format_interval($node->refresh),
    );
    $node->content['simplefeed']['checked'] = array(
      '#value' => $node->checked > 0 ? format_interval(time() - $node->checked) . t(' ago') : t('Never'),
    );
  }

  return $node;
}


/**
 * Theme the display of a feed node.
 */
function theme_simplefeed_node_view($values) {
  // needs to be a space so default values aren't outputed
  $output = ' ';

  // since only administrators can edit when a feed expires, only admins can see what this value is
  if (user_access('administer feeds')) {
    drupal_add_css(drupal_get_path('module', 'simplefeed') .'/simplefeed.css');

    $output .= '<ul class="simplefeed">';
    $output .= '<li><strong>Expires:</strong> '. $values['expires']['#value'] .'</li>';
    $output .= '<li><strong>Refresh:</strong> '. $values['refresh']['#value'] .'</li>';
    $output .= '<li><strong>Checked:</strong> '. $values['checked']['#value'] .'</li>';
    $output .= '</ul>';
  }

  return $output;
}


/**
 * Implementation of hook_link().
 */
function simplefeed_link($type, $node = NULL, $teaser = FALSE) {
  $links = array();

  if ($type == 'node' && $node->type == 'feed') {
    if (module_exists('views')) {
      $links['simplefeed_view_feed_items'] = array(
        'title' => t('View feed items'),
        'href' => 'feeds/'. $node->nid,
        'attributes' => array('title' => t('View feed items')),
      );
    }
    // only admins can force a feed to update or delete all feed items
    if (user_access('administer feeds')) {
      // don't show if the feed is marked not to refresh, since the link won't do anything in this case
      if ($node->refresh > 0) {
        $links['simplefeed_refresh'] = array(
          'title' => t('Refresh this feed'),
          'href' => 'feed/refresh/'. $node->vid,
        );
      }
      $links['simplefeed_delete'] = array(
        'title' => t('Empty this feed'),
        'href' => 'feed/empty/'. $node->nid,
        'attributes' => array("title" => "Delete all feed items associated with this feed")
      );
    }
  }

  return $links;
}


/**
 * Administrative settings.
 */
function simplefeed_admin_settings() {
  $form['feed_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default feed settings'),
    '#description' => t('Default settings for each feed that is added. Note, that some of these settings can be overriden by editing any specific feed node.'),
    '#collapsible' => TRUE,
    '#access' => user_access('administer feeds'),
  );

  $period = array(0 => t('Never')) + drupal_map_assoc(array(3600, 10800, 21600, 32400, 43200, 86400, 172800, 259200, 604800, 1209600, 2419200, 4838400, 9676800), 'format_interval');
  $form['feed_settings']['simplefeed_expires'] = array(
    '#type' => 'select',
    '#title' => t('Discard feed items older than'),
    '#default_value' => variable_get('simplefeed_expires', 86400),
    '#options' => $period,
    '#description' => t('Older feed items will be automatically discarded.  Requires !cron to be running.', array('!cron' => l('cron', 'admin/logs/status'))),
  );

  $period = array(0 => t('Never'), 1 => t('Manual')) + drupal_map_assoc(array(900, 1800, 3600, 7200, 10800, 21600, 32400, 43200, 64800, 86400, 172800, 259200, 604800), 'format_interval');
  $form['feed_settings']['simplefeed_refresh'] = array(
    '#type' => 'select',
    '#title' => t('Check feeds every'),
    '#description' => t('How often should a feed be checked for new content? If you select "manual", you can update a feed by clicking "refresh this feed" below the feed node. If you select a time, !cron must be running.', array('!cron' => l('cron', 'admin/logs/status'))),
    '#options' => $period,
    '#default_value' => variable_get('simplefeed_refresh', 3600),
  );

  foreach (filter_formats() as $id => $format) {
    $formats[$id] = $format->name;
  }
  $form['simplefeed_format'] = array(
    '#type' => 'select',
    '#title' => t('Default input format'),
    '#description' => t('The default input format for feeds (e.g., which HTML tags to not strip out).'),
    '#options' => $formats,
    '#default_value' => variable_get('simplefeed_format', 1),
  );

  // only if taxonomy is enabled will these options work
  if (module_exists('taxonomy')) {
    $vocabularies = taxonomy_get_vocabularies();
    $vocabularies_list = array(0 => 'None');
    foreach ($vocabularies as $vocabulary) {
      $vocabularies_list[$vocabulary->vid] = check_plain($vocabulary->name);
    }

    $form['simplefeed_vocab'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#description' => t("Select a vocabulary that both feeds and feed items share to have feed items automatically inherit their feed parent's terms when created. Select none to disable this feature."),
      '#options' => $vocabularies_list,
      '#default_value' => variable_get('simplefeed_vocab', 0),
    );
    $form['#validate']['simplefeed_settings_validate'] = array();

    $form['simplefeed_categories'] = array(
      '#type' => 'checkbox',
      '#title' => t('Automatically add categories set by external feeds to the vocabulary above.'),
      '#description' => t('This is useful when external feeds provide their own categories. These categories can then be merged into the main vocabulary defined above.'),
      '#default_value' => variable_get('simplefeed_categories', 0),
    );
  }

  $throttle = drupal_map_assoc(array(5, 10, 15, 20, 25, 50, 100, 'unlimited'));
  $form['simplefeed_cron_throttle'] = array(
    '#type' => 'select',
    '#title' => t('Cron throttle'),
    '#description' => t('The number of feeds to parse each cron run.'),
    '#options' => $throttle,
    '#default_value' => variable_get('simplefeed_cron_throttle', 50),
  );

  return system_settings_form($form);
}


/**
 * Validate the SimpleFeed settings.
 */
function simplefeed_settings_validate($form_id, $form_values) {
  // ensure that the vocab selected has been selected to be used by both feed and feed item node types
  if ($form_values['simplefeed_vocab']) {
    $check = array();
    $types = db_query('SELECT type FROM {vocabulary_node_types} WHERE vid = %d', $form_values['simplefeed_vocab']);
    $tags = db_result(db_query('SELECT tags FROM {vocabulary} WHERE vid = %d', $form_values['simplefeed_vocab']));
    while ($type = db_fetch_object($types)) {
      $check[] = $type->type;
    }

    if (!$tags) {
      form_set_error('simplefeed_vocab', t('Automatic term inheritance from parent feed to child feed items requires that this vocabulary has <em>free tagging</em> enabled.'));
    }

    if (!in_array('feed', $check) || !in_array('feed_item', $check)) {
      form_set_error('simplefeed_vocab', t('If you want to use this vocabulary, you need to associate it with both the <em>feed</em> and <em>feed item</em> node types.'));
    }
  }

  // you can't add categories to a non-existent vocab
  if ($form_values['simplefeed_categories'] && !$form_values['simplefeed_vocab']) {
    form_set_error('simplefeed_categories', t('You must specify a valid vocabulary if you want to automatically add categories.'));
  }
}


/**
 * Implementation of hook_cron().
 */
function simplefeed_cron() {
  simplefeed_feed_refresh();
  simplefeed_item_feed_expire();
}


/**
 * Refresh a feed, downloading any new feed items and creating nodes for them.
 */
function simplefeed_feed_refresh($nid = NULL) {
  // figure out which feeds to update
  if (is_numeric($nid)) {
    // refresh a specific feed
    // since a $vid is specified, we are looking at a specific feed node so we allow either "manual" or anytime option to be a valid value to push a refresh
    $process_feeds = db_query('SELECT n.nid FROM {node} n INNER JOIN {simplefeed_feed} s ON s.vid = n.vid WHERE n.nid = %d AND n.status = 1 AND s.refresh > 0', $nid);
  }
  else {
    // refresh all feeds
    $limit = variable_get('simplefeed_cron_throttle', 50);
    // limit in ASC order so we grab the top N that haven't been checked recently
    // we can only update feed nodes that have a valid refresh time set; ignore "never" and "manual" options
    $query = 'SELECT s.nid FROM {simplefeed_feed} s INNER JOIN {node} n ON s.vid = n.vid WHERE ((%d - s.checked) >= s.refresh) AND n.status = 1 AND s.refresh > 1 ORDER BY s.checked ASC';
    if ($limit != 'unlimited') {
      $process_feeds = db_query_range($query, time(), 0, $limit);
    }
    else {
      $process_feeds = db_query($query, time());
    }
  }

  // build cache path
  $cache_path = file_create_path('cache_simplefeed');
  file_check_directory($cache_path, FILE_CREATE_DIRECTORY);

  // process each feed
  while ($process_feed = db_fetch_object($process_feeds)) {
    $process_feed = node_load($process_feed->nid);
    // above we define hook_view() which then performs check_url() on the $url in the feed node
    // the problem is check_url() calls filter_xss_bad_protocol() which does it thing to prevent XSS
    // but it returns the string through check_plain() which calls htmlspecialchars()
    // this converts & in a url to &amp; and then causes SimplePie not to be able to parse it
    // because of this, we decode this URL since we are passing it directly to SimplePie
    // it is still encoded everywhere else it is output to prevent XSS
    $process_feed->url = htmlspecialchars_decode($process_feed->url, ENT_QUOTES);
    $process_feed->tags = '';
    $process_feed->vocab = 0;

    // gather tags for feed to pass to feed item children
    if ($process_feed->vocab = variable_get('simplefeed_vocab', 0)) {
      $terms = taxonomy_node_get_terms_by_vocabulary($process_feed->nid, $process_feed->vocab);
      $taxonomy = array();
      if (!empty($terms)) {
        foreach ($terms as $term) {
           $taxonomy[] = $term->name;
        }
      }
      $process_feed->tags = implode($taxonomy, ', ');
    }

    // turn each feed item into a node
    simplefeed_item_feed_parse($process_feed, $cache_path);

    // finished processing this feed so we can mark it checked
    db_query('UPDATE {simplefeed_feed} SET checked = %d WHERE vid = %d', time(), $process_feed->vid);
  }

  // if you're manually refreshing a feed, redirect back to the feed node
  if ($nid) {
    drupal_set_message(t('Feed refreshed.'));
    drupal_goto('node/'. $nid);
  }
}


/**
 * Delete all feed items associated with a feed.
 */
function simplefeed_feed_empty($nid) {
  if (is_numeric($nid)) {
    $nodes = db_query("SELECT nid FROM {simplefeed_feed_item} WHERE fid = %d", $nid);
    while ($node = db_fetch_object($nodes)) {
      node_delete($node->nid);
    }
    drupal_set_message(t('Feed emptied.'));
    drupal_goto('node/'. $nid);
  }
  else {
    drupal_not_found();
    exit;
  }
}        

/**
 * Implementation of hook_requirements().
 */
function simplefeed_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time.
  $t = get_t();

  if ($phase == 'runtime') {
    $path = drupal_get_path('module', 'simplefeed');
    
    if (!file_exists($path .'/simplepie.inc')) {
      $requirements['simplepie'] = array(
        'title' => $t('SimpleFeed'),
        'value' => $t('SimplePie library missing'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('SimpleFeed requires the <a href="http://simplepie.org/">SimplePie</a> library to properly parse feeds. Please download either the 1.0 or 1.1 development (recommended) version and place simplepie.inc in your SimpleFeed module directory.'),
      ); 
    }
  }
  return $requirements;
}

/**
 * This is a PHP4 compatibility function -- not needed since this is already in PHP5
 */
if (!function_exists("htmlspecialchars_decode")) {
  function htmlspecialchars_decode($string, $quote_style = ENT_COMPAT) {
    return strtr($string, array_flip(get_html_translation_table(HTML_SPECIALCHARS, $quote_style)));
  }
}